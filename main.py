import gym
import numpy as np
import random
import matplotlib.pyplot as plt

env = gym.make('MountainCar-v0')

states_shape = (env.observation_space.high - env.observation_space.low) * [10, 100]
states_shape = np.round(states_shape, 0).astype(int) + 1

Q = np.zeros((states_shape[0], states_shape[1], env.action_space.n))

EPSILON = 0.15
LEARNING_RATE = 0.2
DISCOUNT = 0.9
EPISODES = 5000

sum_rewards = []
avg_rewards = []

for i in range(EPISODES):

	done = False
	
	episode_rewards = []

	curr_state = env.reset()
	curr_state_index = (curr_state - env.observation_space.low) * [10, 100]
	curr_state_index = np.round(curr_state_index, 0).astype(int)

	while not done:

		if i > EPISODES * .999:
			env.render()
		
		if random.uniform(0, 1) < EPSILON:
			action = random.randint(0, env.action_space.n - 1)
		else:
			action = np.argmax(Q[curr_state_index[0], curr_state_index[1]]) 

		next_state, reward, done, info = env.step(action)
		next_state_index = (next_state - env.observation_space.low) * [10, 100]
		next_state_index = np.round(next_state_index, 0).astype(int)

		if done and next_state[0] >= 0.5:
			Q[curr_state_index[0], curr_state_index[1], action] = reward
		else:
			Q[curr_state_index[0], curr_state_index[1], action] += LEARNING_RATE * (reward + DISCOUNT * np.max(Q[next_state_index[0], next_state_index[1]]) - Q[curr_state_index[0], curr_state_index[1], action]) 

		curr_state_index = next_state_index

		episode_rewards.append(reward)

	EPSILON -= EPSILON / EPISODES

	sum_rewards.append(np.sum(episode_rewards))
	avg_rewards.append(np.mean(sum_rewards))

	if ( i + 1 ) % 100 == 0:
		print(str(i + 1) + ': ' + str(np.sum(episode_rewards)))

env.close()

plt.plot(range(EPISODES), avg_rewards)
plt.xlabel('episodes')
plt.ylabel('Average Reward')
plt.title('Average Reward vs episodes')
plt.savefig('rewards_main.png')     
plt.close() 


